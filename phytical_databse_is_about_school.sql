
CREATE DATABASE school;
-- Create tables
CREATE TABLE Students (
    StudentID SERIAL PRIMARY KEY,
    Name VARCHAR(100) NOT NULL,
    Gender CHAR(1) CHECK (Gender IN ('M', 'F')),
    BirthDate DATE CHECK (BirthDate > '2000-01-01')
);

CREATE TABLE Teachers (
    TeacherID SERIAL PRIMARY KEY,
    Name VARCHAR(100) NOT NULL,
    Subject VARCHAR(100)
);

CREATE TABLE Courses (
    CourseID SERIAL PRIMARY KEY,
    Name VARCHAR(100) NOT NULL,
    TeacherID INT REFERENCES Teachers(TeacherID)
);

CREATE TABLE Classrooms (
    ClassroomID SERIAL PRIMARY KEY,
    Location VARCHAR(100) NOT NULL,
    Capacity INT CHECK (Capacity > 0)
);

CREATE TABLE Enrollments (
    StudentID INT REFERENCES Students(StudentID),
    CourseID INT REFERENCES Courses(CourseID),
    UNIQUE(StudentID, CourseID)
);

CREATE TABLE Assignments (
    AssignmentID SERIAL PRIMARY KEY,
    CourseID INT REFERENCES Courses(CourseID),
    Description TEXT,
    DueDate DATE
);

-- Add 'record_ts' field to each table
ALTER TABLE Students ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE Teachers ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE Courses ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE Classrooms ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE Enrollments ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;
ALTER TABLE Assignments ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

-- Insert data
INSERT INTO Students (Name, Gender, BirthDate) VALUES 
('Ibrohim Jalilov', 'M', '2001-01-01'), 
('Dilbar Rahimova', 'F', '2002-02-02'),
('Alice Smith', 'F', '2001-03-03'),
('Bob Johnson', 'M', '2002-04-04'),
('Charlie Brown', 'M', '2001-05-05');

INSERT INTO Teachers (Name, Subject) VALUES 
('Mr. Smith', 'Mathematics'), 
('Ms. Johnson', 'English'),
('Dr. Williams', 'Science'),
('Prof. Davis', 'History'),
('Mrs. Miller', 'Art');

INSERT INTO Courses (Name, TeacherID) VALUES 
('Algebra', 1), 
('Literature', 2),
('Biology', 3),
('World History', 4),
('Painting', 5);

INSERT INTO Classrooms (Location, Capacity) VALUES 
('Building A, Room 101', 30), 
('Building B, Room 201', 25),
('Building C, Room 301', 20),
('Building D, Room 401', 35),
('Building E, Room 501', 40);

INSERT INTO Enrollments (StudentID, CourseID) VALUES 
(1, 1), 
(2, 2),
(3, 3),
(4, 4),
(5, 5);

INSERT INTO Assignments (CourseID, Description, DueDate) VALUES 
(1, 'Solve the algebraic equations.', '2022-12-01'), 
(2, 'Write an essay on Shakespeare.', '2022-12-15'),
(3, 'Complete the lab report.', '2022-12-20'),
(4, 'Write a report on World War II.', '2022-12-25'),
(5, 'Create a self-portrait.', '2022-12-30');

